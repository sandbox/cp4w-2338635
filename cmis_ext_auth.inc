<?php
/**
 * @file
 * cmis_ext_auth.inc 
 */

/**
 * Interface DrupalExtAuthPlugin
 *
 * The goal is to be able to connect to ANY external system, each one with it's own
 * specificities.
 * For ex: we want an external auth to Alfresco, we create a class for alfresco auth that
 * has to implements this interface.
 *
 */
interface CMISExtAuthPlugin {
  function getPluginName();
  function validate($user_name, $password);
  function logoutFromExternal($drupal_uid);
}

/**
 * CMISExtAuth Exception class.
 *
 */
class CMISExtAuthException extends Exception {
}
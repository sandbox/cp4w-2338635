<?php
/**
 * @file
 * cmis_ext_auth_base.inc
 */

abstract class CMISExtAuthBase {
  protected $ext_auth_plugin = NULL;

  function __construct($plugin_name) {
    $this->ext_auth_plugins = variable_get('cmis_ext_auth_plugins', array());

    if (count($this->ext_auth_plugins) > 0 && isset($this->ext_auth_plugins[$this->getPluginName()])) {
      $this->ext_auth_plugin = $this->ext_auth_plugins[$plugin_name];
    }
  }

  function createDrupalUser($login, $password, $email) {
    // Get 'Drupal CMIS user' id
    $drupal_cmis_user_role = user_role_load_by_name('Drupal CMIS user');
    if ($drupal_cmis_user_role == FALSE) {
      // Role doesn't exist - Not normal
      // TODO - Handle error
    }
    else {
      // Doesn't exist, we can create the account
      $fields = array(
        'name' => $login,
        'mail' => $email,
        'pass' => $password,
        'status' => 1,
        'init' => 'email address',
        'roles' => array(
          DRUPAL_AUTHENTICATED_RID => 'authenticated user',
          $drupal_cmis_user_role->rid => TRUE,
        ),
      );

      //the first parameter is left blank so a new user is created
      $account = user_save('', $fields);

      $drupal_user_id = $account->uid;

      return $drupal_user_id;

      /* TODO - Later
      // If you want to send the welcome email, use the following code

      // Manually set the password so it appears in the e-mail.
      $account->password = $fields['pass'];

      // Send the e-mail through the user module.
      drupal_mail('user', 'register_no_approval_required', $email, NULL, array('account' => $account), variable_get('site_mail', 'noreply@example..com'));
      */
    }

    return FALSE;
  }

  function saveExternalAuthentication($external_user_name, $external_password, $drupal_uid) {
    $num_updated = db_update('cmis_ext_auth')
      ->fields(array(
        'external_login' => $external_user_name,
        'external_password' => $external_password,
        // TODO 'last_authentication'
      ))
      ->condition ('drupal_uid', $drupal_uid)
      ->execute();

    if ($num_updated == 0) {
      // If Drupal uid doesn't exist => insert
      $nid = db_insert('cmis_ext_auth') // Table name no longer needs {}
        ->fields(array(
          'external_login' => $external_user_name,
          'external_password' => aes_encrypt($external_password),
          'drupal_uid' => $drupal_uid,
          // TODO
          //'last_authentication' =>
        ))
        ->execute();
    }
  }

  function getExternalAuthentication($drupal_uid) {
    $query = db_select('cmis_ext_auth', 'auth');
    $query->fields('auth', array('external_login', 'external_password', 'last_authentication'));
    $query->condition('auth.drupal_uid', $drupal_uid);
    $result = $query->execute();

    $row = $result->fetchAssoc();
    if ($row == FALSE) {
      return FALSE;
    }
    else {
      $ext_auth = array();
      $ext_auth['user'] = $row['external_login'];
      $ext_auth['password'] = aes_decrypt($row['external_password']);
      $ext_auth['drupal_uid'] = $drupal_uid;
      // TODO $ext_cred['last_auth'] =

      return $ext_auth;
    }
  }

  function useDefaultCMISApiCredentialsWhenNoTicket() {
    return $this->ext_auth_plugin['use_default_cmis_api_user_when_no_ticket'];
  }
  
  function connectAsDrupalUserOnly() {
    return $this->ext_auth_plugin['connect_as_drupal_user_only'];
  }
  
  /**
   * Simply checks in the database if it contains a particular user
   * @param $userName
   */
  function drupalUserNameAndEmail($userName) {
    $result = db_select('users', 'u')
        ->fields('u', array('uid', 'name', 'mail'))
        ->condition('name', $userName)
        ->execute()
        ->fetchAll();
        
    if (count($result) > 0 && isset($result[0]->mail) && !empty($result[0]->mail)) {
      // email can't be blank !
      return $result;
    }
    
    return FALSE;
  }
}
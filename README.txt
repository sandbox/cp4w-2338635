CMIS Repository external authentication module
----------------------------------------------

Contents
--------

 * Requirements
 * Installation
 * Configuration

Requirements
------------

- CMIS API module (https://drupal.org/project/cmis)
- AES Encryption module (https://drupal.org/project/aes)

Installation
------------

Simply copy the module folder in your "modules" folder.

Configuration
-------------

Make sure that:
1. "cmis" and the following sub-modules have to be enabled: "cmis_common", "cmis_browser" and "cmis_query".
2. "aes" module is enabled.

In your "settings.php" file:

1. Make sure that the "CMIS API" module is configured. The "transport" property MUST be set to "cmis_ext_auth".

Example:
$conf['cmis_repositories'] = array(
  'default' => array(
    'user' => '<cmis_user_username>',
    'password' => '<cmis_user_password>',
    'url' => 'http://path/to/cmis/interface',
    'transport' => 'cmis_ext_auth',
  )
);

The 'user' and 'password' are still important, even if we're going to use an "external authentication". When the external authentication will fail,
the user will have the choice (see here under) to use the "default" cmis repository user (eg: to display "public" documents).

2. Configure the CMIS Repo External authentication module:

- Connect to /admin/config/cmis/external_auth

Currently, only an Alfresco plugin is configurable. In the futue, we could think
to something more generic and include other CMIS repositories

- For Alfresco:
> "Base uri" is the Alfresco url webscipt (eg: http://localhost:8080/alfresco/s)
> Set "use_default_cmis_api_user_when_no_ticket" to "FALSE" if you don't want to use the default user, set in the CMIS API configuration. 
  If so, if the external authentication cannot succeed, an error message is shown.


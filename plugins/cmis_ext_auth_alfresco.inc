<?php
/**
 * @file
 * cmis_ext_auth_alfersco.inc
 */

/**
* Class DrupalExtAuthAlfrescoPlugin
*
* Class that validate the connection to an Alfresco repository.
*
* The goal is to get a ticket from Alfresco repo.
*/
class CMISExtAuthAlfrescoPlugin extends CMISExtAuthBase implements CMISExtAuthPlugin {

  protected static $instance = NULL;

  public static function getInstance() {
    if (!isset(static::$instance)) {
      static::$instance = new static();
    }
    return static::$instance;
  }

  function __construct() {
    parent::__construct($this->getPluginName());
  }

  function getPluginName() {
    return 'alfresco';
  }

  /**
   * Validate the external authentication and returns the DRUPAL user id or FALSE if we can't authenticate
   */
  function validate($user_name, $password) {
    // try to get ALfresco ticket
    $alf_ticket = $this->getAlfrescoTicket($user_name, $password);
    $drupal_uid = user_authenticate($user_name, $password);
    
    if ($alf_ticket != FALSE) {
      // We found the ticket
      if ($drupal_uid == FALSE) {
        // Not Drupal user yet
        
        // In order to get the Alfresco infos (e-mail)
        $alf_user = new AlfrescoUser($user_name, $alf_ticket);
        
        // Problem here: if the user name exists in Alfresco and Drupal with different passwords (eg: Drupal: admin/xxxx, Alfresco: admin/yyyy)
        // We are going to compare the e-mails. If the same e-mail addresses, we consider they are the same guys
        // If not the same e-mail, validation must return FALSE, and when can connect it to Drupal only.
        
        // Remark: not using user_load cause for the admin, returns me user 0, without e-mail (would need user 1)
        $drupal_user_same_name = parent::drupalUserNameAndEmail($user_name);
        
        if ($drupal_user_same_name != FALSE) {
          // A user with same user name in Drupal and Alfresco, but with different passwords.
          // We have to determine if same person by checking e-mail address.
          if (isset($alf_user->email) && !empty($alf_user->email) && isset($drupal_user_same_name[0]->mail) 
            && $drupal_user_same_name[0]->mail == $alf_user->email) {
            // E-mails are the same. We can consider they are the same
            $drupal_uid = $drupal_user_same_name[0]->uid;
            
            $this->saveExternalAuthentication('ROLE_TICKET', $alf_ticket, $drupal_uid);
          }
          else {
            // Not considered the same, we can't let the external auth succeed
            drupal_set_message(t('The same user name "@name" already exists in both Drupal and external repository but your e-mail addresses don\'t match. We can\'t give you access for security reasons.', array('@name' => $drupal_user_same_name[0]->name)), 'status');
            return FALSE;
          }
        }
        else {
          $drupal_uid = $this->createDrupalUser($user_name, $password, $alf_user->email);  
          
          $this->saveExternalAuthentication('ROLE_TICKET', $alf_ticket, $drupal_uid);
        }
      }
      else {
        $this->saveExternalAuthentication('ROLE_TICKET', $alf_ticket, $drupal_uid);
      } 

      return $drupal_uid;
    }
    elseif ($drupal_uid != FALSE) {
      // Not registered in Alfresco repo.
      // We have the choice here, and it depends of configuration (see UI admin screen)
      // Or we are allowed to connect a Drupal-only user (with or without default CMIS API user) or not
      if ($this->connectAsDrupalUserOnly()) {
        drupal_set_message(t('An account matching "@name" doesn\'t exist in the external repository. But the configuration allows you to connect as a Drupal user only.', array('@name' => $user_name)), 'status');
        return $drupal_uid;
      }
    }

    return FALSE;
  }

  function getAlfrescoTicket($user_name, $password) {

    // TODO : currently in settings.php, but should be part of an admin screen

    if (isset($this->ext_auth_plugin)) {
      $alfresco_base_uri = $this->ext_auth_plugin['base_uri'];

      // Prepare POST request (also exists with GET)
      $headers = array('Accept' => 'application/json');
      $data = array(
          'username' => $user_name,
          'password' => $password,
      );
      $options = array(
          'headers' => $headers,
          'method' => 'POST',
          'data' => json_encode($data),
      );

      // Raise the request and get the response
      $ws_response = drupal_http_request($alfresco_base_uri . '/api/login', $options);

      if (isset($ws_response->data) && isset(drupal_json_decode($ws_response->data)['data']['ticket'])) {
          return drupal_json_decode($ws_response->data)['data']['ticket'];
      }
      else {
          return FALSE;
      }
    }
    else {
      // No alfresco config
      throw new CMISExtAuthException(t('No plugin configuration found for "' . $this->getPluginName() . '". PLease check your "cmis_ext_auth_plugins" configuration variable in settings.php'));
    }
  }

  /**
   * To logout from Alfresco, we have to delete the ticket
   * Use webscript DELETE /alfresco/s/api/login/ticket/{ticket}
   */
  function logoutFromExternal($drupal_uid) {
    $ext_auth = $this->getExternalAuthentication($drupal_uid);

    if ($ext_auth == FALSE) {
      // If no ticket saved, this is not normal
      drupal_set_message(t('No Alfresco ticket has been found. This is not a normal behaviour'), 'error');
    }
    else {
      // Delete Alfresco ticket
      if (isset($this->ext_auth_plugin)) {
        $alfresco_uri = $this->ext_auth_plugin['base_uri'] . "/api/login/ticket/" . $ext_auth['password'];

        $options = array(
          'method' => 'DELETE',
        );

        $ws_response = drupal_http_request($alfresco_uri . '/api/login/ticket', $options);

        // Remove record in db
        db_delete('cmis_ext_auth')
          ->condition('drupal_uid', $drupal_uid)
          ->execute();

        return TRUE;
      }
      else {
          // Normally not possible. Something's wrong somewhere
        throw new CMISExtAuthException(t('No plugin configuration found for "' . $this->getPluginName() . '". PLease check your "cmis_ext_auth_plugins" configuration variable in settings.php'));
      }
    }

    return FALSE;
  }
}

class AlfrescoUser {

  public $user_name = NULL;
  public $first_name = NULL;
  public $last_name = NULL;
  public $email = NULL;

  // TODO can get a lot more info
/*string(820) "{
	"url": "\/alfresco\/s\/api\/person\/drupal_user",
	"userName": "drupal_user",
	"enabled": true,
	"firstName": "Drupal",
	"lastName": "User",
	"jobtitle": null,
	"organization": null,
	"organizationId": null,
	"location": null,
	"telephone": null,
	"mobile": null,
	"email": "cristophe.pepe@amplexor.com",
	"companyaddress1": null,
	"companyaddress2": null,
	"companyaddress3": null,
	"companypostcode": null,
	"companytelephone": null,
	"companyfax": null,
	"companyemail": null,
	"skype": null,
	"instantmsg": null,
	"userStatus": null,
	"userStatusTime": null,
	"googleusername": null,
	"quota": -1,
	"sizeCurrent": 0,
	"emailFeedDisabled": false,
	"persondescription": null
,
	"capabilities":
	{
		"isMutable":
		true
				,"isGuest":
		false
				,"isAdmin":
		false
			}
}
"
*/

  function __construct($user_name, $ticket) {
    $base_uri = variable_get('cmis_ext_auth_plugins', array());
    $people_ws_uri = $base_uri['alfresco']['base_uri'] . "/api/people/" . $user_name . '?alf_ticket=' . $ticket;

    $headers = array('Accept' => 'application/json');
    $options = array(
      'headers' => $headers,
      'method' => 'GET',
    );

    $ws_response = drupal_http_request($people_ws_uri, $options);

    if (isset($ws_response->data)) {
      $json = drupal_json_decode($ws_response->data);

      $this->user_name = $json['userName'];
      $this->first_name = $json['firstName'];
      $this->last_name = $json['lastName'];
      $this->email = $json['email'];
    }
    else {
      throw new CMISExtAuthException(t('Bad response while calling "' . $people_ws_uri . '"'));
    }
  }
}
<?php 
/**
 * @file
 * cmis_ext_auth.admin.inc 
 */

/**
 * Implements the settings form for each CMIS repositories
 * Currently, only Alfresco avialble. But could be extended to other CMIS repos
 */
function cmis_ext_auth_settings() {
  $form = array();
  
  $form['cmis_ext_auth_plugins_set_info'] = array(
    '#markup' => t('Configure the CMIS repositories authentication parameters'),
  );
  
  $form['cmis_ext_auth_plugins_set'] = array(
    '#title' => t('Alfresco'),
    '#description' => t('Fill the following paremeters in order to connect to an Alfresco repository'),
    '#type' => 'fieldset',
    '#tree' => TRUE,
   );

   $current_config = variable_get("cmis_ext_auth_plugins", NULL);
   
  $form['cmis_ext_auth_plugins_set']['alfresco']['base_uri'] = array(
    '#title' => t('Alfresco web service uri'),
    '#type' => 'textfield',
    '#default_value' => (isset($current_config) && isset($current_config['alfresco']['base_uri']) ? $current_config['alfresco']['base_uri'] : ""),
  );
   
   $form['cmis_ext_auth_plugins_set']['alfresco']['use_default_cmis_api_user_when_no_ticket'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use default CMIS API user when no ext. auth. user found'),
    '#default_value' => (isset($current_config) && isset($current_config['alfresco']['use_default_cmis_api_user_when_no_ticket']) ? $current_config['alfresco']['use_default_cmis_api_user_when_no_ticket'] : FALSE),
    '#description' => t('If a user can\'t be found in the external repository, do we have to use the default user configured in CMIS API module ?'),
   );
   
   $form['cmis_ext_auth_plugins_set']['alfresco']['connect_as_drupal_user_only'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow connection if no matching user in external repository'),
    '#default_value' => (isset($current_config) && isset($current_config['alfresco']['connect_as_drupal_user_only']) ? $current_config['alfresco']['connect_as_drupal_user_only'] : FALSE),
    '#description' => t('If you want to manage users in Drupal, who are not part of the external repository system, check this option. If not checked, the authentication will fail if no user is found in the external repository.'),
   );
   
  return system_settings_form($form);
}

/**
 * Implements form alter
 * 
 * Goal is to setup a custom submit handler
 * 
 * @param unknown_type $form
 * @param unknown_type $form_state
 * @param unknown_type $form_id
 */
function cmis_ext_auth_form_cmis_ext_auth_settings_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'cmis_ext_auth_settings') {
    $form['#submit'][] = 'cmis_ext_auth_save_ext_auth_config_submit';
  }
}

/**
 * Custom submit handler. Goal is to rebuild the settings array variable with the updated values
 */
function cmis_ext_auth_save_ext_auth_config_submit($form, &$form_state) {
  // Alfresco
  $ext_auth_settings = array();
  $ext_auth_settings['alfresco']['base_uri'] = $form_state['values']['cmis_ext_auth_plugins_set']['alfresco']['base_uri'];
  $ext_auth_settings['alfresco']['use_default_cmis_api_user_when_no_ticket'] = $form_state['values']['cmis_ext_auth_plugins_set']['alfresco']['use_default_cmis_api_user_when_no_ticket'];
  $ext_auth_settings['alfresco']['connect_as_drupal_user_only'] = $form_state['values']['cmis_ext_auth_plugins_set']['alfresco']['connect_as_drupal_user_only'];

  variable_set('cmis_ext_auth_plugins', $ext_auth_settings);
  
  // TODO: in the future, maybe other CMIS repo  
}
